# This file is named test_... which will be used by pytest automatically

# Import the pytest module so we can access more advanced functionality.
# You don't need to import pytest if you only use asserts.
import pytest

# Import the module we want to test
import simple_math


# Any function named test_... are detected as tests by pytest
def test_add():
    assert simple_math.add(3, 6) == 9    # assert will pass the test if the expression is True
    assert simple_math.add(2, -4) == -2  # You can have multiple asserts in a test


# You can skip tests that shouldn't be run
@pytest.mark.skip
def test_sub():
    assert simple_math.sub(3, 6) == -3


def test_mul():
    assert simple_math.mul(3, 6) == 18


def test_total():
    numbers = [4, 6, 2, -1, -8, 7]
    assert simple_math.total(numbers) == sum(numbers)


# This test is expected to fail because find() isn't implemented yet
@pytest.mark.xfail
def test_find():
    numbers = [4, 6, 2, -1, -8, 7]

    assert simple_math.find(numbers, 6) == 1
