# This is a simple math library that we want to test with automatic tests.
# This is white box testing because we are directly calling the code under test.

def add(num1, num2):
    return num1 + num2


def sub(num1, num2):
    return num1 + num2  # Copy paste error, will fail test


def mul(num1, num2):
    return num1 * num2


def total(numbers):
    result = 0

    for n in numbers:
        result += n

    return result


# Not implemented yet
def find(numbers, value):
    pass
